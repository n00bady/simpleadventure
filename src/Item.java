import java.io.Serializable;

public class Item implements Serializable {
    // This class is used for any pickable item the player encounters
    // TODO: Might need to add more things like damage, effects etc...
    //       if we want to add combat or other mechanics in the game.

    private String name;
    private String desc;
    private boolean equipment;
    // lazy way to do it
    private boolean consumable;
    private int dmg = 0;
    private int hp = 0;

    public Item() {

    }

    public Item(String name, String desc) {
        this.name = name;
        this.desc = desc;
        equipment = false;
        consumable = false;
    }

    public Item(String name, String desc, boolean equipment, int damage, int hitpoints, boolean consumable) {
        this.name = name;
        this.desc = desc;
        this.equipment = equipment;
        this.consumable = consumable;
        this.dmg = damage;
        this.hp = hitpoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    // return the dmg stat
    public int getDMG() {
        return dmg;
    }

    // return the hp stat
    public int getHP() {
        return hp;
    }

    // check if an item is an equipment type or not
    public boolean getType() {
        return equipment;
    }

    // check if consumable
    public boolean isConsumable() {
        return consumable;
    }

    @Override
    public String toString() {
        return "\u001B[33mItem name= \u001b[0m" + name + "\u001B[33m, description= \u001b[0m" + desc ;
    }



}