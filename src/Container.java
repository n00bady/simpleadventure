import java.io.Serializable;
import java.util.Vector;

public class Container implements Serializable {

    private String name;
    private String description;
    public Vector contents;
    private boolean locked;
    private Item requires;


    public Container() {
        name = "";
        description = "";
        contents = new Vector();
        locked = false;
        requires = null;
    }

    public Container(String n, String d) {
        name = n;
        description = d;
        contents = new Vector();
        locked = false;
        requires = null;

    }

    public Container(String n, String d, Boolean l, Item r) {
        name = n;
        description = d;
        contents = new Vector();
        locked = l;
        requires = r;
    }


    public String getName() {
        return name;
    }

    public void setName(String n){
        name = n;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String d){
        description = d;
    }

    public void addItem(Item i) {
        contents.addElement(i);
    }


    public void removeItem(Item i) {
        if (contents.contains(i)) {
            contents.removeElement(i);
        }
    }

    public Vector open() {
        if (locked) {
            return null;
        }
        return (Vector) contents.clone();
    }

    public Vector getItems() {
        return (Vector) contents.clone();
    }

    public Item getRequires() {
        return (Item) requires;
    }

    public void unlock() {
        locked = false;
    }

}
