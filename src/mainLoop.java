import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

public class mainLoop {
    // TODO: More commands, fix the exceptions, prettify the displaying of all.
    //      Add an actual small scenario instead of just example cases!
    //      better selection recognition for playerInput().

    public static final String filename = "world.sav";  // the name of the save file
    private Room[] rooms;
    private Player p1 = new Player();
    private final int slowdown = 1500; // Used to simulate the time it takes to do an action
    boolean quit = false;
    int turns_count = 0;
    private Room last_room;
    private String prologue;

    public void startLoop() throws Exception {
        // INIT
        // enter main menu sto select if you want to continue or start a new game
        mainMenu();

        // Print prologue if it exists
        if ( prologue != null) {
            System.out.println(prologue);
            System.out.println("\u001B[38;5;199mPress Enter key to continue...\u001b[0m");
            try { System.in.read(); } catch (Exception ignored) {}
        }

        // Main loop Starts here ---
        do {
            // check if player is alive if player is dead end the game
            // TODO: make it so the player can choose to restart or continue from a previous save
            if (p1.getHP() <= 0) {
                System.out.println("\u001b[9mYOU DIED\u001b[0m");
                System.exit(0);
            }

            // display current Room
            displayRoom();
            // get and process player's input
            playerInput();

            // check the equipment
            // maybe there is a better way to do this
            p1.calculateDMG();
            p1.calculateHP();

            // hostiles attack automatically if they have more dmg than the player
            if (!p1.getRoom().getNPC().isEmpty()) {
                for (Enumeration npc = p1.getRoom().getNPC().elements(); npc.hasMoreElements();) {
                    NPC an_npc = (NPC) npc.nextElement();
                    if (an_npc.getAttackable() && an_npc.getDMG() > 0) {
                        combat(an_npc);
                    }
                }
            }

            // winning condition
            if (p1.getRoom() == rooms[15]) {
                displayRoom();
                System.out.println("\u001b[1;32mCongrats you found the exit!!!\u001b[0m");
                System.out.println("Total turns: " + turns_count);
                quit = true;
            }

            // count turns
            turns_count += 1;
        } while (!quit);
    }

    // world initialization
    // In theory, we can have multiple world and initialize whichever we want.
    public void initWorld() throws CloneNotSupportedException {
        World newWorld = new World();
        rooms = newWorld.getRooms();
        p1 = newWorld.getPlayer();
        last_room = p1.getRoom();
        prologue = newWorld.getPrologue();
    }

    // help on how to use some commands
    public void help(){
        System.out.println("The available commands are the following:\n");
        System.out.println("\u001B[33mGO\u001b[0m \u001B[36m<direction>\u001b[0m -> \u001B[33mMoving through the rooms.\u001b[0m");
        System.out.println("\t\u001B[31mExample:\u001b[0m \u001B[33mGO\u001b[0m \u001B[36mWEST\u001b[0m");
        System.out.println("\u001B[33mTAKE\u001b[0m \u001B[36m<item>\u001b[0m -> \u001B[33mTakes an item from the room and adding it to inventory.\u001b[0m");
        System.out.println("\t\u001B[31mExample:\u001b[0m \u001B[33mTAKE\u001b[0m \u001B[36mTEDDY BEAR\u001b[0m");
        System.out.println("\u001B[33mDROP\u001b[0m \u001B[36m<item>\u001b[0m -> \u001B[33mDrops an item from inventory to the current room\u001b[0m");
        System.out.println("\t\u001B[31mExample:\u001b[0m \u001B[33mDROP\u001b[0m \u001B[36mKEY\u001b[0m");
        System.out.println("\u001B[33mLOOK\u001b[0m \u001B[36m<selection>\u001b[0m -> \u001B[33mDisplay the available exits, things, doors of the current room and players inventory.\u001b[0m");
        System.out.println("\t\t\t\t\t\u001B[33mOptions for LOOK are: INV, AROUND, EXITS(E),DOORS(D), THINGS(T) \u001b[0m");
        System.out.println("\t\u001B[31mExample:\u001b[0m \u001B[33mLOOK\u001b[0m \u001B[36mEXITS\u001b[0m OR \u001B[33mLOOK \u001B[36mE\u001b[0m");
        System.out.println("\u001B[33mOPEN\u001b[0m \u001B[36m<door-container>\u001b[0m -> \u001B[33mOpens a door or a container and if its necessary uses a key from inventory.\u001b[0m");
        System.out.println("\t\u001B[31mExample:\u001b[0m \u001B[33mOPEN\u001b[0m \u001B[36mEAST DOOR\u001b[0m");
        System.out.println("\u001b[33mUSE[\u001b[0m \u001b[36m<item-from-inventory>\u001b[0m or \u001b[36m<interactable-thing-from-current-room>\u001b[0m -> \u001b[33mUse an item from inventory or interact with a thing in the current room.\u001b[0m");
        System.out.println("\u001b[33mBACK\u001b[0m -> \u001b[33mGo back to the previous room.");
        System.out.println("\u001b[33mSAVE\u001b[0m -> \u001b[33mSaves the current state of the game\u001b[0m");
        System.out.println("\u001B[33mQUIT\u001b[0m -> \u001B[33mQuits the game and make an autosave\u001b[0m");
    }

    // display the current room title, description and exits
    public void displayRoom() {
        System.out.println();
        System.out.println("HP: " + p1.getHP() + "/" + p1.getMax_HP() + "\tDMG: " + p1.getDMG() + "\nTotal turns: " + turns_count);
        System.out.println(">-------------------Current location-------------------<");
        System.out.println(p1.getRoom().getTitle());
        System.out.println("--------------------Description-------------------------");
        System.out.println(p1.getRoom().getDescription());
        System.out.println(">------------------------------------------------------<");
    }

    // display the exits & doors of the current room
    public void displayExits() {
        //System.out.println(">------------------------------------------------------<");
        System.out.println("\u001B[33mAvailable exits: \u001b[0m");
        for (Enumeration e = p1.getRoom().getExits().elements(); e.hasMoreElements();) {
            Exit an_exit = (Exit) e.nextElement();
            System.out.println("\t" + an_exit.getFullDirectionName());
        }
        System.out.println();
        System.out.println("\u001B[33mAvailable doors: \u001b[0m");
        for (Enumeration d = p1.getRoom().getDoors().elements(); d.hasMoreElements();) {
            Door a_door = (Door) d.nextElement();
            System.out.println("\t" + a_door.getName());
        }
        //System.out.println(">------------------------------------------------------<");
    }

    // display the Items and Things in the current Room
    public void displayItemsAndThings() {
        //System.out.println(">------------------------------------------------------<");
        System.out.println("\u001B[33mAvailable items:\u001b[0m");
        for (Enumeration i = p1.getRoom().getItems().elements(); i.hasMoreElements();) {
            Item an_item = (Item) i.nextElement();
            System.out.println("\t" + an_item.getName());
        }
        System.out.println();
        System.out.println("\u001B[33mAvailable things: \u001b[0m");
        for (Enumeration t = p1.getRoom().getThings().elements(); t.hasMoreElements();) {
            Thing a_thing = (Thing) t.nextElement();
            System.out.println("\t" + a_thing.getName());
        }
        System.out.println();
        System.out.println("\u001b[33mAvailable containers: \u001b[0m");
        for (Enumeration c = p1.getRoom().getContainers().elements(); c.hasMoreElements();) {
            Container a_container = (Container) c.nextElement();
            System.out.println("\t" + a_container.getName());
        }
        //System.out.println(">------------------------------------------------------<");
    }

    // display all NPCs in the current room
    public void displayNPCs() {
        System.out.println("\u001B[33mAvailable NPCs:\u001b[0m");
        for (Enumeration n = p1.getRoom().getNPC().elements(); n.hasMoreElements();) {
            NPC an_npc = (NPC) n.nextElement();
            System.out.println("\t" + an_npc.getName());
        }
        //System.out.println(">------------------------------------------------------<");
    }

    public void displayInventory() {
        System.out.println("⟔---------------------------------------");
        System.out.println(p1.getInvItems()); // TODO: Needs a loop to get only each item's name so it looks good.
        System.out.println("---------------------------------------⟓");
    }

    // display description Item/Thing/Door
    public void displayDesc(Item item) {
        System.out.println(item.getName());
        System.out.print("\t");
        System.out.println(item.getDesc());
    }
    public void  displayDesc(Thing thing) {
        System.out.println(thing.getName());
        System.out.print("\t");
        System.out.println(thing.getDesc());
    }
    public void displayDesc(Door door) {
        System.out.println(door.getName());
        System.out.print("\t");
        System.out.print(door.getDesc());
    }

    public void clearScreen() throws IOException, InterruptedException {
        System.out.println("Clearing screen!");
        String os = System.getProperty("os.name");
        if (os.contains("Windows")) {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } else {
            System.out.println("\033[H\033[2J");
        }
    }

    // Saving game method
    public void saveState() throws IOException {
        FileOutputStream fOut = new FileOutputStream(filename);
        ObjectOutputStream objOUT = new ObjectOutputStream(fOut);
        GameData save = new GameData(rooms, p1, turns_count);
        objOUT.writeObject(save);
    }

    // Main menu? Ask the player if he wants to start a new game or continue
    public void mainMenu() throws Exception {
        boolean complete = false;
        File tempFile = new File(filename);
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Type \u001B[33mnew\u001b[0m to start a new adventure or \u001B[33mcontinue\u001b[0m to continue the from last one:");
            System.out.print(">> ");
            String ans = in.nextLine();
            if (ans.compareToIgnoreCase("new") == 0) {
                System.out.println("Starting new adventure...");
                System.out.println("\u001b[1;36mCreating new World!\u001b[0m");
                // init world
                initWorld();
                System.out.println("Choose your name: ");
                Scanner input = new Scanner(System.in);
                p1.setName(input.nextLine());
                System.out.println("Welcome " + p1.getName() + "!");
                complete = true;
            } else if (ans.compareToIgnoreCase("continue") == 0) {
                // if a save file exists then load the rooms and p1 variables
                if (tempFile.exists()) {
                    System.out.println("\u001b[1;36mSave file found!\u001b[0m");
                    System.out.println("\u001b[1;33mLOADING...\u001b[0m");
                    FileInputStream fin = new FileInputStream(filename);
                    ObjectInputStream objIN = new ObjectInputStream(fin);
                    GameData load_save = (GameData) objIN.readObject();
                    rooms = load_save.getRooms();
                    p1 = load_save.getPlayer();
                    turns_count = load_save.getTotal_turns();
                    System.out.println("Welcome back " + p1.getName() + "!");
                    complete = true;
                } else {
                    System.out.println("Cannot find save file!");
                }
            }
        } while (!complete);
    }

    // combat mode
    public void combat(NPC npc) {
        boolean end = false;
        int rand = ThreadLocalRandom.current().nextInt(100); // escape chance
        Scanner input = new Scanner(System.in);
        System.out.println("A wild " + npc.getName() + " appears!\n");
        do {
            System.out.println("What do you do ? (Type HIT to strike the enemy or FLEE to try and escape.");
            System.out.print(">> ");
            String cmd = input.nextLine();

            String[] words = cmd.split(" ", 2);
            String command = words[0].toUpperCase();

            switch (command) {
                case "HIT" -> {
                    System.out.println("You attack \u001b[33m" + npc.getName() + " for \u001b[31m" + p1.getDMG() + "!\u001b[0m");
                    npc.decreaseHP(p1.getDMG());
                    System.out.println("\u001b[33m" + npc.getName() + " attacks you for \u001b[31m" + npc.getDMG() + "!\u001b[0m");
                    p1.decreaseHP(npc.getDMG());
                    System.out.println("\u001b[31m-----------------\u001b[0m");
                    System.out.println("Your HP is: " + p1.getHP() + "\n");
                }
                case "FLEE" -> {
                    // TODO: This should be improved.
                    System.out.println("\u001b[33mYou try to flee from battle...\u001b[0m");
                    if (rand > 50) {
                        System.out.println("\u001b[92mYou manage to escape!\u001b[0m");
                        p1.setRoom(last_room);
                        end = true;
                    } else {
                        System.out.println("\u001b[91mYou failed to escape!\u001b[0m");
                    }
                }
                default -> {
                    System.out.println("You can't do that!");
                }
            }
            // check if the player and npc are alive end if one of them dies
            if (p1.getHP() <= 0) {
                end = true;
            }
            if (npc.getHP() <= 0) {
                System.out.println("\u001b[92mYou killed " + npc.getName() + "!\u001b[0m");
                // remove npc from room if dead
                p1.getRoom().removeNPC(npc);
                end = true;
            }
        } while (!end);
    }

    //Player input processing and logic
    public void playerInput() {
        boolean success = false;  // probably there is a better way, but this is what I could do...
        Scanner input = new Scanner(System.in);

        // scan input from player
        System.out.print(">> ");
        String cmd = input.nextLine();
        cmd = cmd.toUpperCase();

        String command;     // verb
        String selection;   // noun(s)
        // make a joiner to join the words after the first, is there any other way to do this easier/faster ?
        // Check if user's input is more than one word
        // if it has more than one word then all the words after the
        // first and put it in a single string selection
        StringJoiner joiner = new StringJoiner(" ");
        String[] words = cmd.split("\\s+");
        command = words[0];
        if (words.length > 1) {
            for (int w = 1; w <= (words.length - 1); w++) {
                joiner.add(words[w]);
            }
        }
        selection = String.valueOf(joiner);
        // poor man's debugger
       // System.out.println("Used command: " + command);
       // System.out.println("Selection: " + selection);
        System.out.println();

        // Switch for commands starts here ---
        try {
            switch (command) {
                case "GO":
                    // move player toward the direction he chooses
                    // check if exit exists then move player
                    for (Enumeration e = p1.getRoom().getExits().elements(); e.hasMoreElements(); ) {
                        Exit an_exit = (Exit) e.nextElement();
                        if ((an_exit.getFullDirectionName().compareTo(selection) == 0) ||
                                an_exit.getShortDirectionName().compareTo(selection) == 0) {
                            System.out.println("You move to the " + an_exit.getFullDirectionName() + " room...");
                            // remember the last room
                            last_room = p1.getRoom();
                            p1.setRoom(an_exit.getLeadsTo());
                            success = true;
                        }
                    }
                    if (!success) {
                        System.out.println("You can't go that way!");
                    }
                    Thread.sleep(slowdown);
                    clearScreen();
                    break;
                case "TAKE":
                    // add item to inventory and remove it from the room
                    for (Enumeration i = p1.getRoom().getItems().elements(); i.hasMoreElements(); ) {
                        Item an_item = (Item) i.nextElement();
                        if ((an_item.getName().compareToIgnoreCase(selection) == 0)) {
                            p1.addInvItems(an_item);
                            p1.getRoom().removeItem(an_item);
                            System.out.println("\u001B[33m*Item\u001b[0m " + selection + "\u001B[33m added to inventory*\u001b[0m");
                            success = true;
                        }
                    }
                    if (!success) {
                        System.out.println("You can't take that!");
                    }
                    Thread.sleep(slowdown);
                    clearScreen();
                    break;
                case "DROP":
                    // drop item from inventory and add it to the room
                    ArrayList<Item> inventory = new ArrayList<>(p1.getInvItems());
                    for (Item an_item : inventory) {
                        if (an_item.getName().compareToIgnoreCase(selection) == 0) {
                            p1.removeInvItems(an_item);
                            p1.getRoom().addItem(an_item);
                            success = true;
                            System.out.println("*\u001B[33mItem removed from inventory*\u001b[0m");
                        }
                    }
                    if (!success) {
                        System.out.println("You can't do that!");
                    }
                    Thread.sleep(slowdown);
                    clearScreen();
                    break;
                case "LOOK":
                    // Look commands to print all the stuff in the room
                    if ((selection.equals("AROUND")) || (selection.equals("A")) || (selection.equals(""))) {      // LOOK AROUND
                        System.out.println("\u001B[33mLooking around for details...\u001b[0m\n");
                        Thread.sleep(slowdown * 2);
                        displayExits();
                        displayItemsAndThings();
                        displayNPCs();
                        success = true;
                    }
                    // print all items in the player's inventory
                    if (selection.equals("INVENTORY") || selection.equals("INV") || selection.equals("I")) {     // LOOK INVENTORY
                        displayInventory();
                        success = true;
                    }
                    if (selection.equals("EXITS") || selection.equals("E")) {       // LOOK EXITS
                        displayExits();
                        success = true;
                    }
                    if (selection.equals("THINGS") || selection.equals("T")) {     // LOOK THINGS
                        displayItemsAndThings();
                        success = true;
                    }
                    if (selection.equals("NPCS") || selection.equals("N")) {       // LOOK NPCs
                        displayNPCs();
                        success = true;
                    }
                    if (!success) {
                        System.out.println("There is nothing here...");
                    }
                    // wait a little for the player
                    System.out.println("\u001B[38;5;199mPress Enter key to return in exploration\u001b[0m");
                    try {
                        System.in.read();
                    } catch (Exception ignored) {
                    }
                    break;
                case "OPEN":
                    // Open doors and containers
                    // TODO: Currently to go through a door you need
                    //       to use the open command a second time
                    //       this needs to change so we can either use the go command
                    //       or make the player through automatically when the player opens it
                    // Check for doors
                    for (Enumeration d = p1.getRoom().getDoors().elements(); d.hasMoreElements(); ) {
                        Door a_door = (Door) d.nextElement();
                        if (a_door.getName().compareToIgnoreCase(selection) == 0) {
                            if (a_door.open() == null) {
                                System.out.println("The door is locked!");
                                System.out.println("It requires: " + a_door.getRequires().getName());
                                System.out.println(("Checking inventory for required key."));
                                // probably there is a better way to do this...
                                if (p1.getInvItems().contains(a_door.getRequires())) {
                                    System.out.println("You used the " + a_door.getRequires().getName() + " to unlock the door.");
                                    a_door.unlock();
                                    // item is removed from the inventory after use
                                    p1.removeInvItems(a_door.getRequires());
                                    success = true;
                                }
                            } else {
                                // just go through if it's unlocked
                                System.out.println("You go through the " + a_door.getName() + "...");
                                last_room = p1.getRoom();
                                p1.setRoom(a_door.getExit().getLeadsTo());
                                success = true;
                            }
                        }
                    }
                    // Check for containers
                    for (Enumeration c = p1.getRoom().getContainers().elements(); c.hasMoreElements();) {
                        Container a_container = (Container) c.nextElement();
                        if (a_container.getName().compareToIgnoreCase(selection) == 0) {
                            if (a_container.open() != null) {
                                // if there are items in the container then add all items in
                                // players inventory and remove them from the container
                                for (Enumeration i = a_container.open().elements(); i.hasMoreElements();) {
                                    Item it = (Item) i.nextElement();
                                    p1.addInvItems(it);
                                    a_container.removeItem(it);
                                    System.out.println(it.getName() + " added in inventory.");
                                    success = true;
                                }
                            } else {
                                // TODO: add unlocking logic
                                System.out.println(a_container.getName() + "is locked.");
                                success = true;
                            }
                        }
                    }
                    if (!success) {
                        System.out.println("You can't do that!");
                    }
                    Thread.sleep(slowdown);
                    clearScreen();
                    break;
                case "USE":
                    // TODO: Use is interacting with items and things
                    //      e.g. <use torch> to use the torch item from your inventory
                    //      or <use lever> to use a lever inside the room
                    // check for items in the inventory to use
                    ArrayList<Item> inv = new ArrayList<>(p1.getInvItems());
                    for (Item an_item : inv) {
                        if (an_item.getName().compareToIgnoreCase(selection) == 0) {
                            if (an_item.isConsumable()) {
                                // TODO: make this more generic because this only works for healing items now
                                p1.increaseHP(an_item.getHP());
                                p1.removeInvItems(an_item);
                            }
                            System.out.println("You used " + an_item.getName());
                            success = true;
                        }
                    }
                    // check for things in the current room to use
                    for (Enumeration t = p1.getRoom().getThings().elements(); t.hasMoreElements();) {
                        Thing a_thing = (Thing) t.nextElement();
                        if (a_thing.getName().compareToIgnoreCase(selection) == 0) {
                            System.out.println("You used the " + a_thing.getName());
                            // TODO: Make some things intractable like levers/buttons etc...
                            success = true;
                        }
                    }
                    if (!success) {
                        System.out.println("You can't do that!");
                    }
                    Thread.sleep(slowdown);
                    break;
                case "HELP":
                    // Talk to an NPC
                    help();
                    break;
                case "TALK":
                    // Talk to NPCs
                    for (Enumeration n = p1.getRoom().getNPC().elements(); n.hasMoreElements();) {
                        NPC an_npc = (NPC) n.nextElement();
                        if (an_npc.getName().compareToIgnoreCase(selection) == 0) {
                            System.out.println(an_npc.getDialogue());
                        }
                    }
                    System.out.println("\u001B[38;5;199mPress Enter key to return in exploration\u001b[0m");
                    try {
                        System.in.read();
                    } catch (Exception ignored) {
                    }
                    break;
                case "ATTACK":
                    for (Enumeration npc = p1.getRoom().getNPC().elements(); npc.hasMoreElements();) {
                        NPC an_npc = (NPC) npc.nextElement();
                        if (an_npc.getName().compareToIgnoreCase(selection) == 0 && an_npc.getAttackable()) {
                            combat(an_npc);
                            success = true;
                        }
                    }
                    if (!success) {
                        System.out.println("You can't do that!");
                    }
                    clearScreen();
                    break;
                case "BACK":
                    if (p1.getRoom() != last_room) {
                        System.out.println("You go back, from were you came.");
                        p1.setRoom(last_room);
                    } else {
                        System.out.println("You don't remember were you were before that. ???");
                    }
                    clearScreen();
                    break;
                case "SAVE":
                    // Saving the game TODO: Support for multiple save files.
                    System.out.println("\u001b[1;33mSaving game state...\u001b[0m");
                    saveState();
                    break;
                case "QUIT":
                    // Save and quiting the game
                    System.out.println("\u001b[1;33mSaving game state...\u001b[0m");
                    saveState();
                    System.out.println("\u001b[1;31mTERMINATING...\u001b[0m");
                    // should we use the quit variable here or this is fine ???
                    System.exit(0);
                    break;
                default:
                    // if the command is not recognized tell the player
                    System.out.println("Command " + command + " is not valid.");
                    break;
            }
        } catch (Exception e) {
            // I think you log exception something like this...
            Logger.getLogger("Commands input.").log(Level.INFO, "An exception in the command's switch occurred: ", e);
        }
        // switch end ---
    }
}
