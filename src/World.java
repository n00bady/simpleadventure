public class World {
    // This is the world all the rooms the layout, exits, doors, etc...
    // are placed here.
    // To create a new scenario modify this or create a new similar class
    // and then load in from the mainLoop.initWorld .
    private final Room[] rooms;
    private final Player player = new Player();
    private final String prologue;

    public World() {
        // Optionally add a prologue to appear at the beginning of the game
        prologue = """
                You wake up in the entrance of a grant hall, you don't
                remember much , only that were were falling in an abyss.
                You have to find your way out of here!""";

        // Room creation
        rooms = new Room[16];

        rooms[0] = new Room("Dungeon Entrance", """
                A grant arch stands before you, an eerie darkness spills
                out of it. A friendly ghost stands near it's looking at
                you like it wants to talk to you."""); //key door of Room's 8)
        rooms[1] = new Room("East Side Hall", """
                A small dimly lit corner hall turning north, there is an
                old chest hiding in the dark corner.""");
        rooms[2] = new Room("Waiting room", """
                This seems like a strange waiting room a broken
                statue stand in the middle.""");
        rooms[3] = new Room("Restroom", """
                You almost vomit by the stench, several rows of toilet
                stands all seemingly clogged or overflowing with grey
                water. I hope you don't need to go...""");
        rooms[4] = new Room("Dark Hall", """
                At first glance this dark hall seems to go for ever, but
                after a moment your eyes adjust and you see a door to
                the north and an entrance to the west.""");
        rooms[5] = new Room("Lounge", """
                A big room seems like it used to be a lounge,
                there are several sofas and seats to sit but all seem
                to be broken.""");  // Key of Room's 2)
        rooms[6] = new Room("Broken Tower", "A broken tower it's door locked.");
        rooms[7] = new Room("Dark Hall", "Another dark hall this one seems to go only towards the North.");
        rooms[8] = new Room("Tool Shack", """
                There are a bunch of old broken tools lying around most
                seem unusable. You should search you might find
                something useful.""");  // Key of Room's 6)
        rooms[9] = new Room("Tower Side Room", """
                A small room with a weird door on it's East side.
                There some rumble on the ground with a bunch of broken
                weird instruments.""");  // Key of Room's 7)
        rooms[10] = new Room("Courtyard", """
                A big courtyard it seems that at some point in the past
                there was a beautiful garden here, but now it's barren of
                life only some patches of ghostly ethereal flowers exist
                here.""");
        rooms[11] = new Room("Dark Hall Junction", """
                There is a T junction here the hall seems to go in 3 different directions West, East and South.""");
        rooms[12] = new Room("Small Side Room", """
                A small room that looks like storage room all the
                shelves are empty and broken, cobwebs running between
                them. Something dripping from the ceiling you look up
                and you see a big spider munching on something.
                Maybe you should go back it doesn't look friendly.""");
        rooms[13] = new Room("Hall Junction", """
                Another hall junction this one is lit by weird
                gemstones embedded in different motifs on the walls.
                You notice a magnificent door to the North and
                a smaller hallway to the west.""");
        rooms[14] = new Room("Torture Room", """
                The small swirling hallway leads you to a
                room full of broken desks and seats.
                There is dried blood on the floor and the walls,
                doesn't feel right...""");
        rooms[15] = new Room("Exit Room", "You Found the exit! But where this exit leads to ???");


        // Keys creation
        //Item key2d = new Item ("Key D2", "A small shiny key.");
        Item key7d = new Item ("Key D7", "A small shiny key.");
        Item key8d = new Item ("Key D8", "A small shiny key.");
        Item key6d = new Item ("Key D6", "A small shiny key.");
        Item FinalKey = new Item ("Final Key", "A small shiny key.");
        // Doors creation
        // !!!DO NOT put . at the names of your doors it's throwing off the use command logic!!!
        Door door1 = new Door("Simple door", "There is a half broken sign on it with a kid pissing in a pot.", new Exit(Exit.WEST, rooms[3]));
        Door door2 = new Door("ing Room", "Big old door with a partial fade out sign.", new Exit(Exit.WEST, rooms[2]));
        Door door6 = new Door("East Door", "6th Room", new Exit(Exit.EAST, rooms[6]), true, key6d);
        Door door8 = new Door("North Door", "8th Room", new Exit(Exit.NORTH, rooms[8]), true, key8d);
        Door door7 = new Door("North Door", "7th Room", new Exit(Exit.NORTH, rooms[7]), true, key7d);
        Door doorEND = new Door("North Door", "End Room", new Exit(Exit.NORTH, rooms[15]), true, FinalKey);
        //Door door6 = new Door("Restroom door", "Humans only! says on the door.", new Exit(Exit.EAST, rooms[6]));
        //Door door8 = new Door("Restroom door", "Humans only! says on the door.", new Exit(Exit.NORTH, rooms[8]));

        // Exits for each room if they have a door then the exit is added in the door.(a little stupid way of doing it)
        rooms[0].addExit(new Exit(Exit.NORTH, rooms[4])); // start room exits
        rooms[0].addExit(new Exit(Exit.EAST, rooms[1]));
        rooms[0].addDoor(door2);

        rooms[1].addExit(new Exit(Exit.WEST, rooms[0]));
        rooms[1].addExit(new Exit(Exit.NORTH, rooms[10]));

        rooms[2].addExit(new Exit(Exit.EAST, rooms[0]));
        rooms[2].addDoor(door1);


        rooms[3].addExit(new Exit(Exit.EAST, rooms[2]));

        rooms[4].addExit(new Exit(Exit.SOUTH, rooms[0]));
        rooms[4].addExit(new Exit(Exit.WEST, rooms[5]));
        rooms[4].addDoor(door7);

        rooms[5].addExit(new Exit(Exit.EAST, rooms[4]));

        rooms[6].addExit(new Exit(Exit.WEST, rooms[10]));
        rooms[6].addExit(new Exit(Exit.NORTH, rooms[9]));

        rooms[7].addExit(new Exit(Exit.SOUTH, rooms[4]));
        rooms[7].addExit(new Exit(Exit.NORTH, rooms[11]));

        rooms[8].addExit(new Exit(Exit.SOUTH, rooms[10]));

        rooms[9].addExit(new Exit(Exit.SOUTH, rooms[6]));
        rooms[9].addExit(new Exit(Exit.EAST, rooms[0]));

        rooms[10].addExit(new Exit(Exit.SOUTH, rooms[1]));
        rooms[10].addDoor(door6);
        rooms[10].addDoor(door8);

        rooms[11].addExit(new Exit(Exit.SOUTH, rooms[7]));
        rooms[11].addExit(new Exit(Exit.EAST, rooms[12]));
        rooms[11].addExit(new Exit(Exit.WEST, rooms[13]));

        rooms[12].addExit(new Exit(Exit.WEST, rooms[11]));

        rooms[13].addExit(new Exit(Exit.EAST, rooms[11]));
        rooms[13].addExit(new Exit(Exit.WEST, rooms[14]));
        rooms[13].addDoor(doorEND);

        // add items in rooms
        rooms[0].addItem(key8d);
        //rooms[5].addItem(key2d);
        rooms[9].addItem(key7d);
        rooms[8].addItem(key6d);
        rooms[12].addItem(FinalKey);
        rooms[2].addItem(new Item("Teddy Bear", "A small cute teddy bear."));
        rooms[2].addThing(new Thing("Broken Statue", "A very old broken statue of some long forgotten deity."));

        rooms[3].addThing(new Thing("Bathroom stalls", "Some very dirty bathroom stalls, just from the smell you do not even want to look inside!"));
        rooms[3].addThing(new Thing("Sink", "A small dirty sink, broken on the left corner. Hope you are not thirsty..."));

        rooms[8].addItem(new Item("Skooma", "An illegal narcotic that is used throughout Tamriel. xD"));

        // Add NPC
        rooms[0].addNPC(new NPC("Friendly Ghost", """
                A friendly ghost with a vague humanoid appearance, seems
                like it wants to talk to you.""",
                """
                Welcome to the Starting Abyss Dungeon adventurer! Now go
                and explore and find the exit if you can! But be careful
                the others are not as friendly as me. If you want you
                can try your skills at the training dummy.""",
                false, 100, 5));
        rooms[0].addNPC(new NPC("Training Dummy", "An old training dummy.", "Try your combat skills on me if you dare!", true, 25, 0));
        rooms[12].addNPC(new NPC("Big Spooder", "A big black spider the size of a small child!", "It hisses at you!", true, 10, 4));
        rooms[7].addNPC(new NPC("Ghost Soldier", "A ghostly soldier with tattered armor and an ethereal sword.", "AUGGHHHH!", true, 8, 1));

        // Create a treasure chest
        Container treasure_chest = new Container("Old Chest", "A simple wooded chest, with no lock.");
        // add Items to the treasure chest
        treasure_chest.addItem(new Item("Holy Water Pistol", "A toy water pistol full of holy water, might be useful.", true, 1,0, false));
        treasure_chest.addItem(new Item("Healing Potion", "Heals you, a little.", false, 0, 5, true));
        // Add a treasure chest
        rooms[1].addContainer(treasure_chest);

        // player starting position
        player.setRoom(rooms[0]);

    }

    public Room[] getRooms() {
        return rooms.clone();
    }

    public Player getPlayer() throws CloneNotSupportedException { return player.clone(); }

    public String getPrologue() {
        return prologue;
    }
}
