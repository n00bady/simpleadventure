import java.io.Serializable;

public class GameData implements Serializable {
    // This is a POJO class that helps in saving the rooms and the player state
    // might need to be updated if we make significant changes to the other classes

    private Room s_rooms[];
    private Player s_player;
    private int total_turns;

    public GameData(Room rooms[], Player player, int turns) {
        s_rooms = rooms;
        s_player = player;
        total_turns = turns;
    }

    public Room[] getRooms() {
        return s_rooms;
    }

    public Player getPlayer() {
        return s_player;
    }

    public int getTotal_turns() { return total_turns; }
}
