import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Player implements Serializable, Cloneable {
    // This is the player class anything related to player goes here
    // TODO: If we wanna have battles we need to add some kind of damage or other stats here and
    //          of course ways to set and get them

    private String name;
    private int max_HP = 10;    // max HP
    private int HP;            // current HP
    private Room location = null;
    private List<Item> inventory = new ArrayList<>(); //inventory ArrayList TODO: Needs a limit.
    private int base_dmg = 4;
    private int total_dmg = base_dmg;

    // Blank constructor
    public Player() {
        name = "Player";
        location = null;
        this.calculateDMG();
        this.calculateHP();
        HP = max_HP;
    }

    // Name only constructor
    public  Player(String Name) {
        name = Name;
        location = null;
        this.calculateDMG();
        this.calculateHP();
        HP = max_HP;
    }

    // Full constructor
    public Player(String Name, int health, Room Location){
        name = Name;
        HP = health;
        location = Location;
        this.calculateDMG();
        this.calculateHP();
    }

    // Set name
    public void setName(String Name) {
        name = Name;
    }

    // Get name
    public String getName() {
        return name;
    }

    // Set location
    public void setRoom(Room Location) {
        location = Location;
    }

    // Get location
    public Room getRoom() {
        return location;
    }

    // Set HP
    public void setHP(int hp) {
        HP = hp;
    }

    // Get HP
    public int getHP(){
        return HP;
    }

    // get the MAX_HP
    public int getMax_HP() { return max_HP; }

    // Set increase HP
    public void increaseHP(int heal) {
        HP += heal;
    }

    // Set decrease HP
    public void decreaseHP(int damage) {
        HP -= damage;
    }

    // Add inventory items
    public void addInvItems(Item inv) {
        inventory.add(inv);
    }

    // Remove inventory items
    public void removeInvItems(Item inv) {
        if (inventory.contains(inv)) {
            inventory.remove(inv);
        }
    }

    // Get inventory items
    public ArrayList<Item> getInvItems() {
        //return (Vector)inventory;
        return (ArrayList<Item>)inventory;
    }

    public Player clone() throws CloneNotSupportedException {
        return (Player) super.clone();
    }

    // use the items in players inventory to calculate it
    public void calculateDMG() {
        int add = 0;
        if (!inventory.isEmpty()) {
            for (Item an_item : inventory) {
                if (an_item.getType()) {
                    add += an_item.getDMG();
                }
            }
        }
        total_dmg = base_dmg + add;
    }

    // use the items in players inventory to calculate it
    public void calculateHP() {
        int add = 0;
        if(!inventory.isEmpty()) {
            for (Item an_item : inventory) {
                if (an_item.getType()) {
                    add += an_item.getHP();
                }
            }
        }
        max_HP += add;
    }

    // get the final dmg
    public int getDMG() {
        return total_dmg;
    }

}
