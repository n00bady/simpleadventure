import java.io.Serializable;
import java.util.Vector;

public class Room implements Serializable {
    // This is the room class each room belongs is of this class
    // we use this to create all rooms for our adventure it needs
    // a title a description and the exits(as a Vector).

    private String title;
    private String description;
    private Vector exits;
    private Vector items;
    private Vector things;
    private Vector doors;
    private Vector npc;
    private Vector containers;

    // Blank constructor
    public Room() {
        title = "";
        description = "";
        exits = new Vector();
        items = new Vector();
        things = new Vector();
        doors = new Vector();
        npc = new Vector();
        containers = new Vector();
    }

    // Title only constructor
    public Room(String t) {
        title = t;
        description = "";
        exits = new Vector();
        items = new Vector();
        things = new Vector();
        doors = new Vector();
        npc = new Vector();
        containers = new Vector();
    }

    // Full constructor
    public Room(String t, String d) {
        title = t;
        description = d;
        exits = new Vector();
        items = new Vector();
        things = new Vector();
        doors = new Vector();
        npc = new Vector();
        containers = new Vector();
    }

    // Set title
    public void setTitle(String roomTitle) {
        title = roomTitle;
    }

    // Get title
    public String getTitle() {
        return title;
    }

    // Set Description
    public void setDescription(String roomDescription) {
        description = roomDescription;
    }

    // Get Description
    public String getDescription() {
        return description;
    }

    // Add an Exit
    public void addExit(Exit e) {
        exits.addElement(e);
    }

    // Remove an Exit
    public void removeExit(Exit e) {
        if (exits.contains(e)) {
            exits.removeElement(e);
        }
    }

    // Return vector exits
    public Vector getExits() {
        return (Vector) exits.clone();
    }

    // add an item
    public void addItem(Item i) {
        items.addElement(i);
    }

    // remove an item
    public void removeItem(Item i) {
        if (items.contains(i)) {
            items.removeElement(i);
        }
    }

    // return vector items
    public Vector getItems() {
        return (Vector) items.clone();
    }

    // add an item
    public void addThing(Thing t) {
        things.addElement(t);
    }

    // remove an item
    public void removeThing(Thing t) {
        if (things.contains(t)) {
            things.removeElement(t);
        }
    }

    // return vector items
    public Vector getThings() {
        return (Vector) things.clone();
    }

    // add Door
    public void addDoor(Door D) {
        doors.addElement(D);
    }

    // get Door
    public Vector getDoors() {
        return (Vector) doors.clone();
    }

    // add NPC
    public void addNPC(NPC npc_char) {
        npc.addElement(npc_char);
    }
    // get NPC
    public Vector getNPC() {
        return (Vector) npc.clone();
    }
    // remove NPC
    public void removeNPC(NPC npc_char) {
        npc.removeElement(npc_char);
    }

    // add Container in Room
    public void addContainer(Container container) {
        containers.addElement(container);
    }

    // get Rooms Containers
    public Vector getContainers() {
        return  (Vector) containers.clone();
    }
}
